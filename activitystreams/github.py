import github
from datetime import timezone


def get_person(**kwargs):
    gh = github.Github()
    user = gh.get_user(**kwargs)
    return {
        "@context": "https://www.w3.org/ns/activitystreams",
        "type": "Person",
        "id": user.id,
        "image": {
            "type": "Image",
            "url": user.avatar_url,
        },
        "name": user.name,
        "location": user.location,
        "summary": user.bio,
        "updated": user.created_at,
        "url": user.url,
    }


def get_projects(**kwargs):
    gh = github.Github()
    user = gh.get_user(**kwargs)
    projects = user.get_repos()
    return [{
        "@context": "https://www.w3.org/ns/activitystreams",
        "type": "Object",
        "generator": {
            "type": "Service",
            "name": "GitHub",
            "url": "https://github.com",
        },
        "id": project.url,
        "image": {
            "type": "Image",
            "url": "",  # TODO
        },
        "name": project.name,
        "summary": project.description,
        "updated": project.updated_at.replace(tzinfo=timezone.utc),
        "url": project.homepage if project.homepage else project.html_url,
    } for project in projects]

