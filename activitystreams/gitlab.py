import dateutil.parser

import gitlab
import markdown


def get_person(**kwargs):
    gl = gitlab.Gitlab("https://gitlab.com")
    user_id = gl.users.list(**kwargs)[0].id
    user = gl.users.get(user_id)
    return {
        "@context": "https://www.w3.org/ns/activitystreams",
        "type": "Person",
        "id": user.id,
        "image": {
            "type": "Image",
            "url": user.avatar_url,
        },
        "name": user.name,
        "location": user.location,
        "summary": user.bio,
        "updated": user.created_at,
        "url": user.web_url,
    }


def get_projects(**kwargs):
    gl = gitlab.Gitlab("https://gitlab.com")
    user_id = gl.users.list(**kwargs)[0].id
    user = gl.users.get(user_id)
    projects = user.projects.list()
    return [{
        "@context": "https://www.w3.org/ns/activitystreams",
        "type": "Object",
        "generator": {
            "type": "Service",
            "name": "GitLab",
            "url": "https://gitlab.com",
        },
        "id": project.id,
        "image": {
            "type": "Image",
            "url": project.avatar_url,
        },
        "name": project.name,
        "summary": markdown.markdown(project.description),
        "updated": dateutil.parser.parse(project.last_activity_at),
        "url": project.web_url,
    } for project in projects]

