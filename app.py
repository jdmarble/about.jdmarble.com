from flask import Flask, render_template
import activitystreams.gitlab
import activitystreams.github
app = Flask(__name__)


@app.after_request
def add_header(response):
    response.cache_control.max_age = 604800
    response.cache_control.public = True
    return response


@app.route("/")
def hello():
    return render_template("index.html", **get_context())


def get_context():
    projects = (
            activitystreams.github.get_projects(login="jdmarble") +
            activitystreams.gitlab.get_projects(username="jdmarble")
    )
    return dict(
        person={
            **activitystreams.github.get_person(login="jdmarble"),
            **activitystreams.gitlab.get_person(username="jdmarble"),
        },
        projects=sorted(projects, key=lambda project: project["updated"], reverse=True)
    )


if __name__ == "__main__":
    app.run()
