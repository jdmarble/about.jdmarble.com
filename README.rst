jdmarble.pw
-----------

This deployment follows "Kubernetes: The Surprisingly Affordable Platform for Personal Projects"
(https://www.doxsey.net/blog/kubernetes--the-surprisingly-affordable-platform-for-personal-projects)
blog post pretty closely.

Ingress Firewall Rule
=====================

The backend servers need to accept HTTPS (port 443) connects from Cloudflare only.
Add a firewall rule that accepts connections from
all of the IPv4 ranges listed by Cloudflare at https://www.cloudflare.com/ips-v4 .
It should terminate at all of the nodes running the reverse proxies.

Cloudflare Origin Certificate
=============================

In order for Cloudflare to proxy the backend (origin) servers it connects to them over HTTP.
For this to be secure, it should be encrypted over TLS.
To do this, you need a certificate and key pair.
Cloudflare is configured with the certificate to check that it is connected to the true origin server.
The origin server is configured with the key.

You can generate the certificate using the Cloudflare web interface.
Go to the "Crypto" tab, "Origin Certificates" section, and press the "Create Certificate" button.
Save the certificate as ``cert.pem`` and the key as ``key.pem``.
Load them as a Kubernetes secret with::

  kubectl create secret tls cloudflare-origin-cert --cert=cert.pem --key=key.pem

The reverse proxies running on the cluster will use this secret to serve HTTPS to Cloudflare.
You can delete the ``.pem`` files after creating the secret.

